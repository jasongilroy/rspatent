/* RSK     = 1.1.4    LAST_UPDATE = 1.0.0    */
/* omstage.in 11/10/87 14:55 */
/*
     file:          omstage.in
     author:        Michael L. Gordon
     date:          8-Oct-1987
     facility: Object Stage Facility


     Description:

          Public interface of the object stage facility.

     History:

*/
#ifndef   omstageDEF
#define omstageDEF 1
#ifndef omstoreDEF
#include  <omstre.in>
#endif
#ifndef omstorpDEF
#include  <omstorp.in>
#endif
/* definition of Stage Residence Request */
     typedef enum {
          OStageResidencyNotRequired,   /* not required, store or update on d
          OStageResidencyRequired       /* required, safe store */
     } OStageResidency;

/* Definition of the control id used to correlate stage operations with the
   delivery server
*/

/* function OStageStartUpdate - start of a collection of update or delete
   operations.
   Returns status of the operation.
*/
extern    int  OStageStartUpdate( void );

/* function OStageFindEntry - find the object in the directory.
   Returns pointer to the entry of the null pointer if not found.
*/
extern    OStageDirEntryt     *OstageFindEntry( Objectid * );

/* function OStageEndUpdate - notification of the end of the collection of
   update or delete operations.
   Checkpoints the stage.
   Returns status of the operation.
*/
extern    int  OStageEndUpdate( void );

/* function OStageWriteObject - write the supplied object to the stage.
   The caller supplies a pointer to the buffer containing the object.
   Returns status of the operation.
*/
extern    int  OStageWriteObject( BytePtrt );

/* function OStageAddObject - add the object to the stage.
   The entire object is in the buffer at bufptr.
   Returns status.
*/
extern    int  OStageAddObject( BytePtrt );

/* function OStageDeleteObject - delete the specified object from the stage.
   The object and directory entry are deleted from the stage.
   Returns status.
*/
extern    int  OStageDeleteObject( Objectid * );

/* function OStageReadObject - read the specified object into supplied buffer
   Maximum byte count is supplied, function returns actual count.  If
   truncation would occur, an error status is returned and no read occurs.
extern    int  OStageReadObject( objidptr, maxBytes, retActualBytes, bufPtr )
     Objectid       *objidptr;
     int                 maxBytes;
     int                 *retActualBytes;
     BytePtrt       bufPtr;
*/
extern    int  OStageReadObject( Objectid *, int, int *, BytePtrt );

/* public function OStageOpen - open the specified stage file
   Returns the status of the operation
*/
extern    int  OStageOpen( char * );

/* public function OStageClose - close the stage file
   Returns the status of the operation
*/
extern    int  OStageClose( void );

#endif
/* end of omstage.in */

