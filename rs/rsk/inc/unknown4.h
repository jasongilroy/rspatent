/* RSK     = 1.1.4    LAST_UPDATE = 1.0.0    */
/*
   8/12/87

   New style: free memory is divided into 64K partitions.  For each
   partition there's a freelist.  Each free blcok has <next> and <prev>
   pointers containing the addresses (offsets, really) of the next free
   block and the previous free blcok.  The <prev> pointer of the first
   free block and the <next< pointer of the last free block both point to
   block 0, which is the partition's master control block, forever free.
   This master block's <next> pointer indicates the first free block,
   while it's <prev field points to the last free block.

   Unfree blocks have no <next> or <prev> pointers, but they do have a
   <size> field.  Free blocks have a <size> field too.  This size is the
   number of bytes available in a block for user allocation (in other
   words, the size doesn't include itself, or the next and prev words).
   For allocated blocks the least significant bit of <size> is set to 1;
   for blocks on the free list this bit is always 0.

   Subtlish point: the size of a free block is the number of bytes that
   it can accomodate WHEN IT BECOMES UNFREE -- that is, when it loses its
   <next> and <prev> pointers.  Since an allocated block may always be
   freed at some point, such a block can't be smaller than the smallest
   possible free block.  Therefore the minimum value for any block's
   <size> field is sizeof(FREE_CTL_BLOCK) -sizeof(UNFREE_CTL_BLOCK).
   For instance, this is the <size> value that goes with the master
   control block.

                                    *

   This code should be used with C programs compiled for large model
   only.  It replaces the following large-model runtime library routines:

      calloc()
      free() and _ffree()
      malloc() and _fmalloc()
      realloc() and _expand()

   The function malloc_hi() has been added to work with object caching.
   To use object caching, rename malloc() to mallox().  You can do this
   by making the defined value of OBJ_MGR nonzero.

   We initialize by getting as many partitions as possible from DOS. Some
   of these may not be full 64K partitions.  The constant TINY sets a
   lower bound on the amount of memory we consider adequate for a 
   partition.

   The first memory-manager call in any application MUST be mem_init().
*/

#define STATIC /* Periscope (tm) can't find statics */
#define TINY  4 * 1024L /* skip partitions smaller than this -- see tginit */
#define LOOP while(1)

#include <dos.h>
#include <fcntl.h>
#include <stdio.h>

/*****************************************************************
*                                                                *
*                       TYPEDEFS                                 *
*                                                                *
******************************************************************/

typedef unsigned int WORD;
typedef unsigned long int DOUBLEWORD;
typedef WORD OFFSET;   /* long addresses are stored as OFFSET:SEGMENT */
typedef WORD SEGMENT;
typedef struct
{
   WORD length, iteration;
} LONGEST;

/*
   This is the structure that describes a free block in memory.  The
   <next> member contains the offset of the next free block in the
   current partition, or 0 if there is none.  The <prev> member contains
   the offset of the previous block, or 0.  The segment that goes with
   these offsets is constant for a given partition.  The nth partition's
   segment may be found in the nth element of the global array called
   partition[].

   Each partition contains a header block at offset 0.  This block is the
   root of the free list.  Its <next. field points to the first free
   block; its <prev> field points to the last free block in the list.  If
   no blocks are available, <next> and <prev> are both 0.

   The <size> field tells how many bytes of storage are available when the
   block is in use (unfree).  Note that this size doesn't include the storage
   taken up by the control block itself.
*/
typedef struct
{
   WORD size; /* how many bytes of storage available (must be even) */
   OFFSET next;  /* ptr to next block in freelist -- segment in partition[] */
   OFFSET prev;  /* ptr to prev block */
} FREE_CTL_BLOCK, far *FREE_BLOCK_PTR;

/*
   The header of an allocated block is simply a word giving its size.
   Two further points: the least significant bit of <size> is set to show
   that this block isn't free; and <size> MUST be at least
   sizeof(FREE_CTL_BLOCK) - sizeof(UNFREE_CTL_BLOCK), so the block can be
   returned to the freelist.  (That is, it must have room for the forward
   and back pointers.)
*/
typedef struct
{
   WORD size; /* number of bytes in use + 1 */
} UNFREE_CTL_BLOCK, far *UNFREE_BLOCK_PTR;

/*
   This union is a kludge, used to gain speed of access when we're doing
   pointer arithmetic with objects of type FREE_BLOCK_PTR or UNFREE_BLOCK_PTR
   (See above.)  It's used throughout ttxget(), ttxfree() and ttxget_hi().
*/
typedef union
{
   FREE_BLOCK_PTR fullptr;
   struct
   {
      OFFSET off;
      SEGMENT seg;
   } halfptr;
} POINTER;

/*
   Record in the binary tree used to do statistics on sizes.
   (See count_allocs().)
*/
typedef struct me
{
   WORD val;
   DOUBLEWORD count; /* how many times such a block reserved */
   long int current_count;   /* no. such blocks currently allocated */
   DOUBLEWORD high_water; /* max allocations at any one time */
   struct me *left, *right;
} TREE_REC;

/*
   Counters used to gather statistics for summit().
   Notice that this is guaranteed initialization to binary 0's.
*/
typedef struct
{
   DOUBLEWORD bytes_currently_allocated;
   DOUBLEWORD calloc_invocations;
   DOUBLEWORD _expand_invocations;
   DOUBLEWORD _ffree_invocations;
   DOUBLEWORD _fmalloc_invocations;
   DOUBLEWORD _free_invocations;
   DOUBLEWORD FREE_MEMORY_invocations;
   DOUBLEWORD GET_MEMORY_invocations;
   DOUBLEWORD high_water_mark;   /* most bytes ever in use at one time */
   DOUBLEWORD malloc_invocations;
   DOUBLEWORD malloc_obj_invocations;
   DOUBLEWORD max_requested;   /* largest single hunk requested */
   DOUBLEWORD realloc_invocations ;
   DOUBLEWORD total_allocations; /* total ttxget() invocations */
   DOUBLEWORD total_bytes_allocated;
   DOUBLEWORD total_bytes_freed;
   DOUBLEWORD total_frees; /* total free() invocations */
   TREE_REC *root;   /* for allocation-size statistics */
} globs;

/*
   Record in the linear list used to remember what allocation
   sizes you're looking for.

   The idea is to find out where in the code under analysss there
   are calls for blocks of a particular size.  Who wants 8 bytes,
   who wants 23 bytes, etc.  We put the list of wanted bytes into
   SIZEFILE in free format.  Sizes are strings of digits separated
   by whitespace or nondigits:

      00000008,   456 123,0,1,3
      17  ??2

   Don't use the same size twice in this list, or it'll be counted
   twice.
*/
typedef struct my
{
   WORD val;
   struct my *next;
} SIZE;

/*****************************************************************
*                                                                *
*                         CONSTANTS                              *
*                                                                *
******************************************************************/

/*
   We have to test for the largest possible allocation.  Microsoft
   allows 0xffed bytes.  Our limit's higher.
*/
#define MAX_MALLOC_SIZE 0xfff8
#define MAX 8                    /* max number of 64K partitions */
#define EPSILON 8       /* MUST be multiple of 2 -- see ttxget() */
#define Verify 1                  /* see mem_dump() and verify() */
#define Dump Verify + 1                                 /* ditto */
#define SMASH_CONSTANT 17  /* used where SMASH_MEMORY is defined */
/* buf size for local allocations -- see alloc() */
#define LOCAL_BUFSIZE  /*800*/ 400 * sizeof(TREE_REC)
#define ADD 1 /* for use with SIZEFILE */
#define NUL 1 /* for use with SIZEFILE */
#define SUM 1 /* for use with SIZEFILE */

/*
   These tags are used to identify different reported items
   in the logfile.  Procedure: read a tag.  On the basis of the
   tag, read appropriate parameters from logfile.
*/
#define ERROR 1                                       /* tag for all errors */
#define MEM_ALLOC 2              /* tag for memory allocation advisory info */
#define REALLOC 3
#define DUMP 4
#define AVGALLOC 5
#define AVGFREES 6
#define CALLOCS 7
#define _EXPANDS 8
#define _F_CALLOCS 9
#define _FFREES 10
#define _FMALLOCS 11
#define FREE_MEMORYS 12
#define FREES 13
#define GET_MEMORYS 14
#define MALLOCS 15
#define MALLOC_OBJS 16
#define REALLOCS 17
#define TOTFREES 18
#define TOTMEM 19
#define TOTMEMFREED 20
#define TOTREQS 21
#define HIGH_WATER_MARK 22
#define TRAP_VALUE 23
#define MAX_REQUESTED 24
#define SIZES 25
#define MEM_RANGE 26
#define TARGET_SIZE 27

/* error tags for use with log() */
#define ZERO_SIZE_GET         0
#define ZERO_SIZE_FREE        1
#define FREE_OUT_OF_BOUNDS    2
#define WILD_REALLOC          3
#define FREED_REALLOC         4
#define ALREADY_FREE          5
#define TGINIT_FAILED         6
#define OUT_OF_MEMORY         7
#define CTL_BLOCK_GARBAGE     8
#define ZERO_SIZE_REALLOC     9
#define GET_TOO_BIG          10
#define NOT_GOOD_MEMORY      11
#define TRASHED_MEMORY       12
#define WILD_FREE            13
#define INVALID_FREE         14

#define USAGE 1
#define ADVISORY 2
#define UNFREED 4
#define TARGETED 8
#define BLOCK_SIZES 16
#define MEM_WALK 32

#define LINT_ARGS 1
#define LOOP while(1)

#define S_IREAD 0000400         /* read permission, owner */
#define S_IWRITE 0000200        /* write permission, owner */

/*****************************************************************
*                                                                *
*                         MACROS                                 *
*                                                                *
******************************************************************/

#define min(a,b)   (((a) < (b)) ? (a) : (b))
#define max(a,b)   (((a) > (b)) ? (a) : (b))

/*
   Macros for semaphore lock.  This may not be a necessary
   feature.

   8/24/87 -- this feature was removed.  Code left here for
   purposes of documentation.

#define WAIT_ON_SEMAPHORE     while (conch) PREEMPT()
#define CLAIM_SEMAPHORE          conch = 1
#define RELEASE_SEMAPHORE        conch = 0
*/

/*
   All memory blocks begin and end on even boundaries.  This means they
   always have a size that's a multiple of 2.  We use the LSB to show
   whether or not the block is free -- 0 if it is, else 1.  The following
   macro masks that bit off so we can get the true length for use in
   pointer arithmetic.
*/
#define LEN_VAL(len) (len & ~1)

/*
   Macro to turn a conventional CS:IP address into an absolute integer
   address (doubleword).
   The argument is in fact a far address in the usual format, CS:IP.
*/
#define ABSOLUTELY(addr)   (FP_SEG(addr)*0x10L+FP_OFF(addr))


/*****************************************************************
*                                                                *
*                   COMPILE-TIME SWITCHES                        *
*                                                                *
******************************************************************/

/*
   8/26/87: It's been decided to put diagnostics and consistency checks
   into the debugger version only.  Therefore many of the switches are
   AND'ed with the DEBUG switch (by multiplication of constants).  LH.
*/

/*
   If SMASH_MEMORY is nonzero, write a constant into freed blocks and
   check for that constant when you get a new block.

   7/31/87 -- this hasn't been incorporated in free.asm.
*/
#define SMASH_MEMORY 0
#define STATS 0 /* log errors, etc. */
/* CONSISTENCY_CHECK changed from 0 to 1 by Les H, 10/1/87 */
#define CONSISTENCY_CHECK 1 /* check blocks for consistency */
/*
     STATISTICS enables logging of statistics when set to nonzero.
     If you do use it, be sure to link with mallocc.obj and freec.obj
     rather than with malloc.obj and free.obj -- in other words, you
     MUST recompile and link with mallocc.c and free.c and malloc_hi.c.
*/
#define STATISTICS 0     /* enables logging of statistics; LH, 11/5/87 */

/*
     If OBJ_MGR is nonzero, object cacheing is enabled -- calls
     to malloc_obj() cause objects to be stored in high memory.
*/
#define OBJ_MGR 1

/*****************************************************************
*                                                                *
*                         PROTOTYPES                             *
*                                                                *
******************************************************************/

/*
   Here are the prototype declarations.  Notice that we're assuming large
   model -- eg, malloc() returns a far pointer.
*/
#if STATISTICS
char far * malloc_ (WORD, void far *);
char far * malloc_hi (WORD, void far *);
char far * mallox (WORD, void far *);
STATIC char far * abs_to_seg(DOUBLEWORD);
STATIC char far * alloc(WORD);
STATIC SIZE * get_sizes(WORD);
STATIC TREE_REC *add_2_tree(TREE_REC *, WORD);
STATIC TREE_REC *install(WORD);
STATIC void init_logfile(void);
STATIC void log(WORD, void far *);
STATIC void print_tree(TREE_REC *);
STATIC void shuffle(void far *);
STATIC void stat(WORD, WORD, void far *);
STATIC void summum(WORD, DOUBLEWORD);
STATIC WORD is_it_ok(WORD, WORD);
STATIC WORD sub_from_tree(TREE_REC *, WORD);
void far * who_r_u(void);
void free_ (char far * , void far *);
#else
char far * mallox (WORD);
char far * malloc_hi (WORD);
STATIC void shuffle(void);
#endif
char far * GET_MEMORY (WORD);
char far * _expand (char far *, WORD);
char far * _fmalloc (WORD);
char far * calloc (WORD, WORD);
char far * malloc (WORD);
char far * realloc (char far * );
STATIC int tginit(void);
void Consistency_check(void);
void consistency_check(SEGMENT);
void ffree (char far * );
void _ffree (char far * );
void longest_free_list(WORD);
void Memset(char far *, WORD, WORD);
void memsnap(void);
void show_addr(char far *, FILE *);
WORD _msize(void far *);
WORD mem_init(void);

