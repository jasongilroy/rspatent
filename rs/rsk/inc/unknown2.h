/* RSK     = 1.1.4    LAST_UPDATE = 1.0.0    */
extern   null_rtn();

/* Define DM error table. */

ECB  dm_err_table[] =
{
"Display manager: malloc error",null_rtn,
"Display manager: exceded legal limit for # of open windows",null_rtn,
"Display manager: out of memory trying to open window",null_rtn,
"Display manager: window was not open when attempted to save",null_rtn
};
