/* RSK     = 1.1.4    LAST_UPDATE = 1.0.0    */
/********************************************************************
*
*    File name:     dia.inc
*
*********************************************************************
*    Description:   transmit and receive message formats
*
*
*    Revision History:
*
*    Date           Name      Reason
*    -----               ----      ------
*    8/30/85        mel bellar     create
*
*********************************************************************/

#define   REQ_CODE  0              /* request code              */
#define   SEQ_NUM        1              /* number for end to end snc */
#define OBJ_OFFSET  2              /* offset of object id in an */
                                        /*  object request           */
#define   FLAGS1         2              /* control status flags      */
#define FLAGS2      3                   
#define   BLK_NUM         4             /* current block number      */
#define   BLK_CNT         5             /* number of blocks in this  */
                                        /*  message transmission     */
#define   SB_LEN          4             /* single block data length  */
#define   SB_DATA         6             /* single block data start   */
#define   MB_LEN          6             /* multiple block data length*/ 
#define   MB_DATA         8             /* multiple block data start */
